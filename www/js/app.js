var app = angular.module('csapl', ['ionic'])

.config(['$httpProvider', '$ionicConfigProvider', function($httpProvider, $ionicConfigProvider) {
 $httpProvider.defaults.transformRequest.push(function(data) {
        var requestStr;
        if (data) {
            data = JSON.parse(data);
            for (var key in data) {
                if (requestStr) {
                    requestStr += "&" + key + "=" + data[key];
                } else {
                    requestStr = key + "=" + data[key];
                }
            }
        }
        return requestStr;
    });

    $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    $httpProvider.defaults.useXDomain = true;

    $ionicConfigProvider.tabs.style('striped');


}])

.run(function($ionicPlatform, $rootScope, $location, AuthService, $state, $ionicHistory) {

  /*$rootScope.$on( "$locationChangeStart", function(event, next, current) {
      var path = $location.path();
      var allowed_paths = ['/login', '/forgot', '/received-code'];


      
      var i = allowed_paths.length;
      var found = false;
      if(!AuthService.isAuthenticated()) {

        while (i--) {
              if (allowed_paths[i] == path) {
                 found = true;
              } 
        }

        if(!found) {
          $state.go('login');
        }
      }

     
     /* if ($rootScope.loggedInUser == null) {
        // no logged user, redirect to /login
        if ( next.templateUrl === "partials/login.html") {
        } else {
          $location.path("/login");
        }
      }*/
    //});*/

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins) {
      if (window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
    }
    
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
})
.constant('CONFIG', {
  apiURL : "http://espisdev3.com/csaplapi/"
  //apiURL : "http://192.168.60.134/csaplapi/"
  //apiURL : "http://192.168.60.136/csaplapi/"
  //apiURL: "http://34.215.238.64/",
})
 
.constant('USER_ROLES', {
  0: 'admin',
  1: 'manager',
  2: 'customer',
  3: 'user',
});

app.controller('sideBarCtrl', ['$scope', 'AuthService', '$state' , '$ionicHistory', '$ionicSideMenuDelegate', 'CONFIG', '$http', function($scope, AuthService, $state, $ionicHistory, $ionicSideMenuDelegate, CONFIG, $http) {
  

  $scope.role = AuthService.role();
  $scope.user = AuthService.user();
  
  $scope.logout = function() {
      AuthService.logout();
    $state.go('login', {}, {reload: true});
    $ionicHistory.clearHistory();
    $ionicHistory.clearCache();
  }

  $scope.exitApp = function() {
    $http.post(CONFIG.apiURL+'logout');
    ionic.Platform.exitApp();
  };
}]);