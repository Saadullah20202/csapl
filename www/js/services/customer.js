app.factory('CustomerService', ['$rootScope', '$http', '$q', 'CONFIG', 'AuthService', function($rootScope, $http, $q, CONFIG, AuthService) {
	
	var customers = localStorage["customers"];

	var lastUpdated = localStorage["customersUpdateTime"] || '';

	var orderList = localStorage["orderList"];

	var payments = localStorage["customer_payments"];
	
	if(customers) {
		try {
			customers = JSON.parse(customers);
		} catch(e) {
			customers = [];	
		}
	} else {
		customers = [];
	}

	if(orderList) {
		try {
			orderList = JSON.parse(orderList);
		} catch(e) {

			orderList = {};	
		}
	} else {
		orderList = {};
	}

	if(payments) {
		try {
			payments = JSON.parse(payments);
		} catch(e) {

			payments = {};	
		}
	} else {
		payments = {};
	}


	return {
		get customers() {  return customers;},
		get lastUpdated() { return lastUpdated;},
		getSingle : getSingle,
		customerOrders: customerOrders,
		orderDetail : orderDetail,
		customerPayments: customerPayments,
		paymentDetail : paymentDetail,
		orderListFromServer: orderListFromServer,
		paymentsFromServer: paymentsFromServer,
		updateFromServer : updateFromServer,
		saveList: saveList,
	}

	function getSingle (index) {
		return customers[index] || false;
	}

	function updateFromServer(record) {
		if(record) {
			return updateSingleFromServer(record);
		} else {
			return updateAllFromServer();
		}
	}

	function saveList() {
		var dt = new Date();
		lastUpdated = dt;
		localStorage["customersUpdateTime"] = dt;
    localStorage["customers"] = JSON.stringify(customers);

    $rootScope.$broadcast('customers.update');

  }

  function updateSingleFromServer(record) {
  	var defer = $q.defer();
  	if(!record) {
  		defer.reject("error");
  	} else {

  			$http.post(CONFIG.apiURL+"customers").then(function(data) {
		  			console.log("success data");
		  			console.log(data);
		  			defer.resolve("success");
		  		},function(data) {
		  			console.log("failure data");
		  			console.log(data);
		  			defer.reject("error");
		  		}
		  	);

  	}
  	
  	return defer.promise;
  }


  function updateAllFromServer() {
  	var defer = $q.defer();

  	$http.post(CONFIG.apiURL+"customers").then(function(data) {

  			if(data.data.customers) {

  				var records = data.data.customers;
  				
	        customers = records;


	        saveList();

  				defer.resolve(data);

  			} else {
  				defer.reject(error);
  			}
  			

  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }


  function customerOrders(customer_id) {
  	if(!customer_id) {
  		return false;
  	}
		return orderList[customer_id] || [];
	}

	function orderDetail(customer_number, order_index) {
		if(!customer_number || !order_index) {
			return false;
		}
		return orderList[customer_number][order_index] || {};
	}

	function customerPayments(customer_id) {
  	if(!customer_id) {
  		return false;
  	}

		return payments[customer_id] || [];
	}

	function paymentDetail(customer_number, payment_index) {
		if(!customer_number || !payment_index) {
			return false;
		}
		return payments[customer_number][payment_index] || {};
	}


	function orderListFromServer(customer_id) {
  	
	  	if(!customer_id) {
	  		return false;
	  	}

	  	var defer = $q.defer();

	  	$http.get(CONFIG.apiURL+"customer/"+customer_id+"/orders").then(function(data) {
	  		
	  			if(data.data.detail) {
	  				
		        detail = data.data.detail;
		        if(!orderList) {
		        	orderList = {}
		        }
		        orderList[customer_id] = detail;
		      	localStorage["orderList"] = JSON.stringify(orderList);
	  				defer.resolve(data);

	  			} else {
	  				defer.reject(data);
	  			}
	  			
	  		},function(error) {
	  			defer.reject(error);
	  		}
	  	);

	  	return defer.promise;
	}

	function paymentsFromServer(customer_id) {
  	
	  	if(!customer_id) {
	  		return false;
	  	}

	  	var defer = $q.defer();

	  	$http.get(CONFIG.apiURL+"customer/"+customer_id+"/payments").then(function(data) {
	  		
	  			if(data.data.detail) {
	  				
		        detail = data.data.detail;
		        if(!payments) {
		        	payments = {}
		        }
		        payments[customer_id] = detail;
		      	localStorage["customer_payments"] = JSON.stringify(payments);
	  				defer.resolve(data);

	  			} else {
	  				defer.reject(data);
	  			}
	  			
	  		},function(error) {
	  			defer.reject(error);
	  		}
	  	);

	  	return defer.promise;
	}



}]);