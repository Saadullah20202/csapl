app.factory('PaymentService', ['$rootScope', '$http', '$q', 'CONFIG', 'AuthService', function($rootScope, $http, $q, CONFIG, AuthService) {
	
	var payments = localStorage["payments"];

	var lastUpdated = localStorage["paymentsUpdateTime"] || '';

	var details = localStorage["paymentdetail"];
	
	if(payments) {
		try {
			payments = JSON.parse(payments);
		} catch(e) {
			payments = [];	
		}
	} else {
		payments = [];
	}

	if(details) {
		try {
			details = JSON.parse(details);
		} catch(e) {
			details = {};	
		}
	} else {
		details = {};
	}


	return {
		get payments() {  return payments;},
		get lastUpdated() { return lastUpdated;},
		getSingle : getSingle,
		paymentDetail: paymentDetail,
		detailFromServer:	detailFromServer, 
		updateFromServer : updateFromServer,
		saveList: saveList,
	}

	function getSingle (index) {
		return payments[index] || false;
	}

	function updateFromServer(ref) {
	
		return updateAllFromServer(ref);

	}

	function saveList() {
		var dt = new Date();
		lastUpdated = dt;
		localStorage["paymentsUpdateTime"] = dt;
    localStorage["payments"] = JSON.stringify(payments);

    $rootScope.$broadcast('payments.update');

  }



  function updateAllFromServer(ref) {
  	var defer = $q.defer();

  	$http.post(CONFIG.apiURL+"payments", ref).then(function(data) {

  			if(data.data.payments) {
  				
	        payments = data.data.payments;
	        
	        saveList();

  				defer.resolve(data);

  			} else {
  				defer.reject(error);
  			}
  			

  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }


  function paymentDetail(payment_id) {
  	if(!payment_id) {
  		return false;
  	}

		return details[payment_id] || [];
	}



  function detailFromServer(payment_id) {
  	
  	if(!payment_id) {
  		return false;
  	}

  	var defer = $q.defer();

  	$http.get(CONFIG.apiURL+"order/"+payment_id).then(function(data) {
  		

  			if(data.data.detail) {
  				
	        detail = data.data.detail;

	        if(!details) {
	        	details = {}
	        }
	        
	        details[detail.TRX_NUMBER] = detail


	      	localStorage["paymentdetail"] = JSON.stringify(details);
  				
  				defer.resolve(data);

  			} else {
  				defer.reject(data);
  			}
  			

  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }



}]);