app.service('AuthService', ['$http', '$q','USER_ROLES', 'CONFIG', function($http, $q, USER_ROLES, CONFIG) {
  var username = '';
  var isAuthenticated = false;
  var role = '';
  var authToken;
  var user = '';
 
  function loadUserCredentials() {
    var _d = window.localStorage.getItem('user');
    if (_d) {
      try {
        var _d = JSON.parse(_d);
        useCredentials(_d);
      } catch (e) {

      }
      
    }
  }
 
  function storeUserCredentials(data) {
    if(data) {
      var _obj = JSON.stringify(data);
      window.localStorage.setItem('user', _obj);
      useCredentials(data);
    }
    
  }
 
  function useCredentials(data) {
    user = data;
    username = data.Username;
    isAuthenticated = true;
    authToken = data.token;
    var _rl = data.Role
    role = USER_ROLES[_rl];
    // Set the token as header for your requests!
    $http.defaults.headers.common['X-Auth-Token'] = data.token;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    username = '';
    role = '';
    isAuthenticated = false;
   	$http.defaults.headers.common['X-Auth-Token'] = undefined;
     localStorage.removeItem('user');
  }
 
  var login = function(email, pw) {
    var obj = {email: email, password: pw}
    return $http.post(CONFIG.apiURL+'login', obj);
  };


 
  var logout = function() {
    $http.post(CONFIG.apiURL+'logout');
    destroyUserCredentials();
    localStorage["users"] = undefined;
    localStorage["customers"] = undefined;
    localStorage["employees"] = undefined;
    localStorage["orderList"] = undefined;
    localStorage["customer_payments"] = undefined;
    localStorage["payments"] = undefined;
    localStorage["orders"] = undefined;
    localStorage["csusers"] = undefined;
  };
 
  var isAuthorized = function(authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }
    return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
  };

  var reAuth = function() {
    var defer = $q.defer();
    var _d = window.localStorage.getItem('user');
    if (!_d) {
      defer.reject({ message: "invalid" });
      console.log('invalid1');
    } else {
      _d = JSON.parse(_d);
    }
    login(_d._user,  _d._key).
        then(
        function(response) {
           if(response.data.error === true) {
              defer.reject({ message: "invalid" });
              console.log('invalid');
           } else if(response.data.error === false) {
              storeUserCredentials(response.data.user);
              defer.resolve({ message: "success" });
              console.log('sucess');
           } else {
            defer.reject({ message: "invalid" });
            console.log('reject');
           }
        }, function(response) {
          defer.reject({ message: "invalid" });
          console.log('invalid2');
      });

      return defer.promise;
  };
 
  loadUserCredentials();
 
  return {
  	storeUser : storeUserCredentials,
    login: login,
    token: authToken,
    logout: logout,
    isAuthorized: isAuthorized,
    reAuth : reAuth,
    isAuthenticated: function() {return isAuthenticated;},
    user: function() {return user;},
    username: function() {return username;},
    role: function() {return role;}
  };
}]);