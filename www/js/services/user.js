app.factory('UserService', ['$rootScope', '$http', '$q', 'CONFIG', 'AuthService', function($rootScope, $http, $q, CONFIG, AuthService) {
	
	var users = localStorage["users"]; //customer admins

	var employees = localStorage["employees"]; //admin employees

  var csusers = localStorage["csusers"]; //customer users

	var lastUpdated = localStorage["usersUpdateTime"] || ''; //customer admin time

	var employeesLastUpdated = localStorage["employeesUpdateTime"] || '';

  var csusersUpdateTime = localStorage["csusersUpdateTime"] || '';
	
	if(users) {
		try {
			users = JSON.parse(users);
		} catch(e) {
			users = [];	
		}
	} else {
		users = [];
	}

	if(employees) {
		try {
			employees = JSON.parse(employees);
		} catch(e) {
			employees = [];	
		}
	} else {
		employees = [];
	}

  if(csusers) {
    try {
      csusers = JSON.parse(csusers);
    } catch(e) {
      csusers = []; 
    }
  } else {
    csusers = [];
  }


	return {
		get users() {  return users;},
		get employees() {  return employees;},
    get csusers() {  return csusers;},
		get lastUpdated() { return lastUpdated;},
		get employeesLastUpdated() { return employeesLastUpdated;},
    get csusersUpdateTime() { return csusersUpdateTime;},
		getSingle : getSingle,
		getSingleEmployee : getSingleEmployee,
    getSingleCSuser : getSingleCSuser,
		updateFromServer : updateFromServer,
		employeesFromServer : employeesFromServer,
    csUsersFromServer : csUsersFromServer,
		saveList: saveList,
    processForgot: processForgot,
		saveEmployees: saveEmployees,
		handleAccount: handleAccount,
		handleEmployee: handleEmployee,
    handleCsUser : handleCsUser,
    changePass: changePass
	}

	function getSingle (index) {
		return users[index] || {};
	}

	function getSingleEmployee (index) {
		return employees[index] || {};
	}

  function getSingleCSuser (index) {
    return csusers[index] || {};
  }


	function updateFromServer(record) {
		if(record) {
			return updateSingleFromServer(record);
		} else {
			return updateAllFromServer();
		}
	}

	function saveList() {
		var dt = new Date();
		lastUpdated = dt;
		localStorage["usersUpdateTime"] = dt;
    localStorage["users"] = JSON.stringify(users);
    $rootScope.$broadcast('users.update');
  }

  function saveEmployees() {
		var dt = new Date();
		lastUpdated = dt;
		localStorage["employeesUpdateTime"] = dt;
    localStorage["employees"] = JSON.stringify(users);

    $rootScope.$broadcast('employees.update');

  }

  function saveCsUsers() {

    var dt = new Date();
    lastUpdated = dt;
    localStorage["csusersUpdateTime"] = dt;
    localStorage["csusers"] = JSON.stringify(csusers);

    $rootScope.$broadcast('csusers.update');

  }

  function updateSingleFromServer(record) {
  	if(!record) {
  		return;
  	} 
  }

  function handleAccount(user) {
  	var defer = $q.defer();
  	$http.post(CONFIG.apiURL+"customer-admin/account", user).then(
  		function(data) {
  			defer.resolve(data);
  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }

  function handleEmployee(record) {
  	var defer = $q.defer();
  	$http.post(CONFIG.apiURL+"employee/account", record).then(
  		function(data) {
  			defer.resolve(data);
  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }

  function handleCsUser(record) {

    var defer = $q.defer();
    $http.post(CONFIG.apiURL+"csuser/account", record).then(
      function(data) {
        defer.resolve(data);
      },function(error) {
        defer.reject(error);
      }
    );
    return defer.promise;

  }


  function updateAllFromServer() {
  	var defer = $q.defer();

  	$http.post(CONFIG.apiURL+"customer-admins").then(function(data) {

  			if(data.data.users) {

  				var records = data.data.users;
  				
	        users = records;


	        saveList();

  				defer.resolve(data);

  			} else {
  				defer.reject(data);
  			}
  			

  		},function(data) {
  			defer.reject(data);
  		}
  	);
  	return defer.promise;
  }

  function employeesFromServer() {
  	var defer = $q.defer();

  	$http.post(CONFIG.apiURL+"employees").then(function(data) {

  			if(data.data.employees) {

  				var records = data.data.employees;
  				
	        employees = records;


	        saveEmployees();

  				defer.resolve(data);

  			} else {
  				defer.reject(data);
  			}
  			

  		},function(data) {
  			defer.reject(data);
  		}
  	);
  	return defer.promise;
  }

  function csUsersFromServer(customer) {
    var defer = $q.defer();

    $http.post(CONFIG.apiURL+"csusers", customer).then(function(data) {

        if(data.data.users) {
          var records = data.data.users;
          csusers = records;
          saveCsUsers();
          defer.resolve(data);
        } else {
          defer.reject(data);
        }

      },function(data) {
        defer.reject(data);
      }
    );
    return defer.promise;
  }

  function processForgot(user, action) {
    var defer = $q.defer();
    $http.post(CONFIG.apiURL+action, user).then(function(data) {
      var resp = data.data;
      if(resp.error === false) {
        defer.resolve(resp);
      } else {
        defer.reject(resp);
      }
    }, function(error) {

    })
    return defer.promise;
  }

  

  function changePass(data) {
    var defer = $q.defer();
    $http.post(CONFIG.apiURL+"change_pass", data).then(function(data) {
      var resp = data.data;
      if(resp.error === false) {
        defer.resolve(resp);
      } else {
        defer.reject(resp);
      }
    }, function(error) {

    })
    return defer.promise;
  }



}]);