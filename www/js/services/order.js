app.factory('OrderService', ['$rootScope', '$http', '$q', 'CONFIG', 'AuthService', function($rootScope, $http, $q, CONFIG, AuthService) {
	
	var orders = localStorage["orders"];

	var lastUpdated = localStorage["ordersUpdateTime"] || '';

	var details = localStorage["orderdetails"];
	
	if(orders) {
		try {
			orders = JSON.parse(orders);
		} catch(e) {
			orders = [];	
		}
	} else {
		orders = [];
	}

	if(details) {
		try {
			details = JSON.parse(details);
		} catch(e) {
			details = {};	
		}
	} else {
		details = {};
	}


	return {
		get orders() {  return orders;},
		get lastUpdated() { return lastUpdated;},
		getSingle : getSingle,
		orderDetail: orderDetail,
		detailFromServer:	detailFromServer, 
		updateFromServer : updateFromServer,
		saveList: saveList,
	}

	function getSingle (index) {
		return orders[index] || false;
	}

	function updateFromServer() {
			return updateAllFromServer();
	}

	function saveList() {
		var dt = new Date();
		lastUpdated = dt;
		localStorage["ordersUpdateTime"] = dt;
    localStorage["orders"] = JSON.stringify(orders);

    $rootScope.$broadcast('orders.update');

  }


  function updateAllFromServer() {
  	var defer = $q.defer();

  	$http.post(CONFIG.apiURL+"orders").then(function(data) {

  			if(data.data.orders) {
  				
	        orders = data.data.orders;
	        
	        saveList();

  				defer.resolve(data);

  			} else {
  				defer.reject(error);
  			}
  			

  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }


  function orderDetail(order_id) {
  	if(!order_id) {
  		return false;
  	}

		return details[order_id] || [];
	}



  function detailFromServer(order_id) {
  	
  	if(!order_id) {
  		return false;
  	}

  	var defer = $q.defer();

  	$http.get(CONFIG.apiURL+"order/"+order_id).then(function(data) {
  		

  			if(data.data.detail) {
  				
	        detail = data.data.detail;

	        if(!details) {
	        	details = {}
	        }
	        
	        details[detail.ORDERNO] = detail


	      	localStorage["orderdetails"] = JSON.stringify(details);
  				
  				defer.resolve(data);

  			} else {
  				defer.reject(data);
  			}
  			

  		},function(error) {
  			defer.reject(error);
  		}
  	);
  	return defer.promise;
  }



}]);