app.config(function($stateProvider, $urlRouterProvider) {

  var user =  window.localStorage.getItem('user');

  $stateProvider

  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })
  .state('forgot', { 
    url: '/forgot/:email',
     templateUrl: 'templates/forgot.html',
      controller: 'forgotCtrl'
  })
  .state('received-code', {
      url: '/received-code',
      templateUrl: 'templates/received-code.html',
      controller: 'forgotCtrl'
  })
    .state('tabs', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html",
      controller: "tabsController",
      
    })
    .state('tabs.user-types', {
      url: "/user-types",
      views: {
        'users-tab': {
          templateUrl: "templates/users/types.html",
          controller : "TypesCtrl"
        }
      }
    })
    .state('tabs.customer-admins', {
      url: "/customer-admins",
      views: {
        'users-tab': {
          templateUrl: "templates/users/customer-admins.html",
          controller : "CustomerAdminCtrl"
        }
      }
    })
    .state('tabs.create-user', {
      url: "/user/create/:user",
      views: {
        'users-tab': {
          templateUrl: "templates/users/create-user.html",
          controller: "createUser"
        }
      }
    })
    .state('tabs.employees', {
      url: "/employees",
      views: {
        'users-tab': {
          templateUrl: "templates/users/employees.html",
          controller : "EmployeesCtrl"
        }
      }
    })
    .state('tabs.create-employee', {
      url: "/employee/create/:user",
      views: {
        'users-tab': {
          templateUrl: "templates/users/create-employee.html",
          controller: "createEmployee"
        }
      }
    })

    .state('tabs.customer-users', {
      url: "/customer-users",
      views: {
        'customer-users-tab': {
          templateUrl: "templates/users/customer-users.html",
          controller : "customerUsers"
        }
      }
    })
    .state('tabs.create-csuser', {
      url: "/csuser/create/:csuser",
      views: {
        'customer-users-tab': {
          templateUrl: "templates/users/create-csuser.html",
          controller: "createCsuser"
        }
      }
    })
    
    .state('tabs.customers', {
      url: "/customers",
      views: {
        'customers-tab': {
          templateUrl: "templates/customers/customers.html",
          controller: 'CustomersCtrl'
        }
      }
    })
    .state('tabs.customer', {
      url: "/customers/:customer",
      views: {
        'customers-tab': {
          templateUrl: "templates/customers/customer.html",
          controller: 'CustomerDetailCtrl'
        }
      }
    })
    .state('tabs.customer-orders', {
      url: "/customers/:customer/orders",
      views: {
        'customers-tab': {
          templateUrl: "templates/customers/orders.html",
          controller: 'customerOrdersCtrl'
        }
      }
    })
    .state('tabs.customer-order', {
      url: "/customers/:customer/orders/:order",
      views: {
        'customers-tab': {
          templateUrl: "templates/customers/order.html",
          controller: 'CustomerOrderDetailCtrl'
        }
      }
    })
    .state('tabs.customer-payments', {
      url: "/customers/:customer/payments",
      views: {
        'customers-tab': {
          templateUrl: "templates/customers/payments.html",
          controller: 'customerPaymentsCtrl'
        }
      }
    })
    .state('tabs.customer-payment', {
      url: "/customers/:customer/payments/:payment",
      views: {
        'customers-tab': {
          templateUrl: "templates/customers/payment.html",
          controller: 'CustomerPaymentDetailCtrl'
        }
      }
    })
    .state('tabs.orders', {
      url: "/orders",
      views: {
        'orders-tab': {
          templateUrl: "templates/orders/orders.html",
          controller: 'OrdersCtrl'
        }
      }
    })

    .state('tabs.order-detail', {
      url: "/order/:order",
      views: {
        'orders-tab': {
          templateUrl: "templates/orders/detail.html",
          controller: "OrderDetailCtrl"
        }
      }
    })

    .state('tabs.payments', {
      url: "/payments",
      views: {
        'payments-tab': {
          templateUrl: "templates/payments/payments.html",
          controller: 'PaymentsCtrl'
        }
      }
    })

    .state('tabs.payment-detail', {
      url: "/payment/:payment",
      views: {
        'payments-tab': {
          templateUrl: "templates/payments/detail.html",
          controller: "PaymentDetailCtrl"
        }
      }
    })

    .state('tabs.desclaimer', {
      url: "/desclaimer",
      views: {
        'pages-tab': {
          templateUrl: "templates/desclaimer.html",
        }
      }
    })
    .state('tabs.account', {
      url: "/account",
      views: {
        'pages-tab': {
          templateUrl: "templates/account.html",
          controller: "accountCtrl"
        }
      }
    })

    .state('logout', {
      url: '/logout',
      controller: "LogoutCtrl",
      templateUrl: "templates/logout.html",
      
     
  });
   $urlRouterProvider.otherwise("/login");

});