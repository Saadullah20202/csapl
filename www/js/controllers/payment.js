app.controller('PaymentsCtrl', ['$scope', 'PaymentService', 'AuthService', '$ionicNavBarDelegate', '$ionicLoading', function($scope, PaymentService, AuthService, $ionicNavBarDelegate, $ionicLoading) {

	$scope.paymentService = PaymentService;

  $scope.user = AuthService.user();
  $scope.restriction = $scope.user.Restriction;

  $scope.Ref = {RefId: $scope.user.RefId};

  $scope.listlength = 10;

  $scope.loadMore = function(){
    if (!$scope.paymentService.payments){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }

    if ($scope.listlength < $scope.paymentService.payments.length) {
      $scope.listlength+=5;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }


  $scope.hasFiltered = false;


  $scope.addValueToSearch  = function() {
    var val =  document.querySelector('#paymentSearch').value;
    $scope.search = val;
  }




  $scope.hasFocus = false;

   $scope.setFocus = function(){
    $scope.hasFocus = true;
    $ionicNavBarDelegate.title('');
    setTimeout(function(e) {
      document.querySelector('#paymentSearch').focus();
    },100);
   

  };

  $scope.noFocus = function(){
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Payment List');
    if(document.querySelector('#paymentSearch').value.length > 0) {
      $scope.hasFiltered = true;
    };
    $scope.search = '';
  }

  $scope.$on('$ionicView.leave', function() {
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Payment List');
    $scope.search = '';
  });

  $scope.$on('$ionicView.enter', function() {
    
    $ionicLoading.show({
      template: 'Loading...',
    });

    PaymentService.updateFromServer($scope.Ref).then(
      function() {
        $ionicLoading.hide();
      },
      function (error) {
        $ionicLoading.hide();
        //loadAppErrorHandler(error);
        
      }
    );
  });

}]);

app.controller('PaymentDetailCtrl', ['$scope', 'PaymentService', '$ionicNavBarDelegate', '$stateParams', function($scope, PaymentService, $ionicNavBarDelegate, $stateParams) {

  $scope.paymentService = PaymentService;

  var paymentIndex = $stateParams.payment;

  $scope.payment = PaymentService.getSingle(paymentIndex);


}]);