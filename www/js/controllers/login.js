app.controller('LoginCtrl', ['$scope', 'AuthService', '$location', '$ionicPopup', '$state', '$ionicPlatform', '$ionicHistory', function($scope, AuthService, $location, $ionicPopup, $state, $ionicPlatform, $ionicHistory ) {


     var backbtn = $ionicPlatform.registerBackButtonAction(function(e) {
        var path = $location.path();
        if(path == '/login') {
          ionic.Platform.exitApp();
        } else {
          $ionicHistory.goBack();
        }
    }, 100);

   

   /* $scope.$on('$ionicView.leave', function() {
       $ionicPlatform.offHardwareBackButton();
    });*/


    $scope.checkStatus = function() {

      if(AuthService.isAuthenticated()) {

        if(AuthService.role() == 'admin') {
          $state.go('tabs.customers');
        } else if(AuthService.role() == 'manager') {
          $state.go('tabs.customers');
        } else if(AuthService.role() == 'customer') {
          $state.go('tabs.orders');
        } else if(AuthService.role() == 'user') {
          $state.go('tabs.orders');
        }
        
      }

    }

    $scope.$on('$ionicView.enter', function() {
      $scope.checkStatus();
    });

    $scope.showPassword = false;

    $scope.displayPassword = function() {
      if(!$scope.showPassword) {
        $scope.showPassword = true;
      }
    }

    $scope.hidePassword = function() {
      if($scope.showPassword) {
        $scope.showPassword = false;
      }
    }

    $scope.data = {email: '', password:''};

    $scope.login = function() {
      AuthService.login($scope.data.email, $scope.data.password).
        then(
          
        function(response) {

           if(response.data.error === true) {
              var response = response.data;
              if(response.msg != "") {
                var alertPopup = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: response.msg,
                });
              }
           } else if(response.data.error === false) {
            $scope.data = {email: '', password:''};
           	AuthService.storeUser(response.data.user);
            $scope.checkStatus();
            //$state.go('tabs.user-types');

           }
        }, function(response) {
          var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Login failed, please try later.'
            });
      });
    }
}]);

app.controller('LogoutCtrl', ['$scope', 'AuthService', '$state', function($scope, AuthService, $state) {

   AuthService.logout();
     $state.go('login');
    AuthService.logout();
 
}]);

app.controller('forgotCtrl', ['$scope', 'UserService', '$ionicPopup', '$state', '$stateParams', function($scope, UserService, $ionicPopup, $state, $stateParams) {

  $scope.data = {"email" : "", "code": ""};

  $scope.user_email  = $stateParams.email;
  if($scope.user_email) {
    $scope.data.email = $scope.user_email;
  }

  $scope.forgot = function() {
    UserService.processForgot($scope.data, "forgot").then(

    function(data) {
      $state.go('received-code');
    }, function(error) {
      if(error.msg) {
        $ionicPopup.alert({
                title: 'Error!',
                template: error.msg,
            });
      }
    });
  };

  $scope.verify = function() {

     UserService.processForgot($scope.data, "verify").then(
      function(data) {
        $ionicPopup.alert({
                title: 'Success!',
                template: data.msg,
            });
         $state.go('login');

      }, function(failed) {

        if(failed.msg) {
            $ionicPopup.alert({
              title: 'Error!',
              template: failed.msg,
            });
        }
      });

  };
 
}]);


