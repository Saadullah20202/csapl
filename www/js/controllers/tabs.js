app.controller('tabsController', ['$scope', '$state' , 'AuthService', function($scope, $state, AuthService) {
	if(!AuthService.isAuthenticated()) {
		$state.go('login');
		return false;
	}
	$scope.role = AuthService.role();

	$scope.$on('$ionicView.enter', function() {
		$scope.role = AuthService.role();
  });

}]);