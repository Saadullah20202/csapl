app.controller('TypesCtrl', ['$scope', 'AuthService', function($scope,  AuthService ) {

 $scope.user = AuthService.user();
 $scope.role = AuthService.role();
 
}]);

app.controller('CustomerAdminCtrl', ['$scope', 'UserService', '$ionicNavBarDelegate', '$ionicLoading', function($scope, UserService, $ionicNavBarDelegate, $ionicLoading) {

  $scope.userService = UserService;
  $scope.hasFiltered = false;
  $scope.listlength = 10;

  $scope.loadMore = function(){
    if (!$scope.userService.users){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }
    if ($scope.listlength < $scope.userService.users.length) {
      $scope.listlength+=5;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }

  $scope.addValueToSearch  = function() {
    var val =  document.querySelector('#userSearch').value;
    $scope.search = val;
  }

  $scope.hasFocus = false;
   $scope.setFocus = function(){
    $scope.hasFocus = true;
    setTimeout(function(e) {
      document.querySelector('#userSearch').focus();
    },100);
   
    $ionicNavBarDelegate.title('');

  };

  $scope.noFocus = function(){
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Customer Admin');
    if(document.querySelector('#userSearch').value.length > 0) {
      $scope.hasFiltered = true;
    };
    $scope.search = '';
  }

  $scope.$on('$ionicView.leave', function() {
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Customer Admin');
    $scope.search = '';
  });

	$scope.$on('$ionicView.enter', function() {
    $ionicLoading.show({
      template: 'Loading...',
    });
    UserService.updateFromServer().then(
      function() {
        $ionicLoading.hide();
      },
      function (error) {  
        $ionicLoading.hide();      
      }
    );
  });
}]);

app.controller('EmployeesCtrl', ['$scope', 'UserService', '$ionicNavBarDelegate', '$ionicLoading', function($scope, UserService, $ionicNavBarDelegate, $ionicLoading) {

  $scope.userService = UserService;
  
  $scope.listlength = 10;

  $scope.loadMore = function(){
    if (!$scope.userService.employees){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }
    if ($scope.listlength < $scope.userService.employees.length) {
      $scope.listlength+=5;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }



  $scope.$on('$ionicView.enter', function() {
    $ionicLoading.show({
      template: 'Loading...',
    });
    UserService.employeesFromServer().then(
      function() {
        $ionicLoading.hide();
      },
      function (error) {
        $ionicLoading.hide();   
      }
    );
  });
}]);

app.controller('createUser', ['$scope', '$ionicPopup', '$ionicLoading', '$stateParams', 'UserService', '$ionicHistory', function($scope, $ionicPopup, $ionicLoading, $stateParams, UserService, $ionicHistory ) {

  var userIndex = $stateParams.user;

  $scope.master = UserService.getSingle(userIndex);
  $scope.user = angular.copy($scope.master);
  $scope.user.index = userIndex;

  if($scope.master.Active == 1) {
    $scope.user.Active = true;
  } else if(!$scope.master.hasOwnProperty('Active')) {
    $scope.user.Active = true;
  } 

  $scope.handleAccount  = function() {
    $ionicLoading.show({
      template: 'Processing...',
    });
    
    UserService.handleAccount($scope.user).then(

      function(data) {$ionicLoading.hide();
        if(data.data) {
          var data = data.data;
          if(data.error == false) {
            $ionicHistory.goBack();
          } else {

            $ionicPopup.alert({
                    title: 'Error!',
                    template: data.msg,
                });
          }
        }
      }, 
      function(error) {
        console.log(error); $ionicLoading.hide();
        if(error.status == 0) {
          console.log('no internet connectivity available');
        }
      }

    );

  }
 
}]);

app.controller('createEmployee', ['$scope', '$ionicPopup', '$ionicLoading', '$stateParams', 'UserService', '$ionicHistory', function($scope, $ionicPopup, $ionicLoading, $stateParams, UserService, $ionicHistory ) {

  var userIndex = $stateParams.user;

  $scope.master = UserService.getSingleEmployee(userIndex);
  $scope.user = angular.copy($scope.master);
  $scope.user.index = userIndex;

  if($scope.master.Active == 1) {
    $scope.user.Active = true;
  } else if(!$scope.master.hasOwnProperty('Active')) {
    $scope.user.Active = true;
  } 


  $scope.user.ViewOrders = true;
  $scope.user.ViewPayments = true; 

  if($scope.master.Restriction == 1) {
    $scope.user.ViewOrders = false;
  }
  if($scope.master.Restriction == 2) {
    $scope.user.ViewPayments = false;
  }
  if($scope.master.Restriction == 3) {
    $scope.user.ViewOrders = false;
    $scope.user.ViewPayments = false; 
  } 

  $scope.handleAccount  = function() {

    $ionicLoading.show({
      template: 'Processing...',
    });

    UserService.handleEmployee($scope.user).then(

      function(data) { $ionicLoading.hide();
        if(data.data) {
          var data = data.data;
          if(data.error == false) {
            $ionicHistory.goBack();
          } else {

            $ionicPopup.alert({
                    title: 'Error!',
                    template: data.msg,
                });
          }
        }
      }, 
      function(error) { $ionicLoading.hide();
        if(error.status == 0) {
          console.log('no internet connectivity available');
        }
      }

    );

  }
 
}]);


app.controller('customerUsers', ['$scope', 'UserService', 'AuthService', '$ionicNavBarDelegate', '$ionicLoading', function($scope, UserService, AuthService, $ionicNavBarDelegate, $ionicLoading) {

  $scope.user = AuthService.user();
  $scope.UserId = {RefId: $scope.user.RefId};
  $scope.userService = UserService;

  $scope.listlength = 10;

  $scope.loadMore = function(){
    if (!$scope.userService.csusers){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }
    if ($scope.listlength < $scope.userService.csusers.length) {
      $scope.listlength+=5;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }



  $scope.$on('$ionicView.enter', function() {
    $ionicLoading.show({
      template: 'Loading...',
    });
    UserService.csUsersFromServer($scope.UserId).then(
      function() {
        $ionicLoading.hide();
      },
      function (error) {
        $ionicLoading.hide();       
      }
    );
  });
}]);

app.controller('createCsuser', ['$scope', '$ionicPopup', '$ionicLoading', '$stateParams', 'UserService', '$ionicHistory', function($scope, $ionicPopup, $ionicLoading, $stateParams, UserService, $ionicHistory ) {

  var csuser  = $stateParams.csuser;

  $scope.master = UserService.getSingleCSuser(csuser);

  $scope.user = angular.copy($scope.master);

  $scope.user.ParentId = $stateParams.user;

  $scope.user.index = csuser;

  
  if($scope.master.Active == 1) {
    $scope.user.Active = true;
  } else if(!$scope.master.hasOwnProperty('Active')) {
    $scope.user.Active = true;
  } 

  $scope.user.ViewOrders = true;
  $scope.user.ViewPayments = true; 

  if($scope.master.Restriction == 1) {
    $scope.user.ViewOrders = false;
  }
  if($scope.master.Restriction == 2) {
    $scope.user.ViewPayments = false;
  }
  if($scope.master.Restriction == 3) {
    $scope.user.ViewOrders = false;
    $scope.user.ViewPayments = false; 
  }

  $scope.handleAccount  = function(user) {
    $ionicLoading.show({
      template: 'Processing...',
    });
    UserService.handleCsUser(user).then(

      function(data) {    $ionicLoading.hide();
        if(data.data) {
          var data = data.data;
          if(data.error == false) {
            $ionicHistory.goBack();
          } else {

            $ionicPopup.alert({
                    title: 'Error!',
                    template: data.msg,
                });
          }
        }
      }, 
      function(error) {
        $ionicLoading.hide();
        if(error.status == 0) {
          console.log('no internet connectivity available');
        }
      }

    );

  }
 
}]);

app.controller('accountCtrl', ['$scope', 'AuthService','UserService','$ionicPopup', '$state', '$ionicHistory','$ionicLoading', function($scope,  AuthService, UserService, $ionicPopup, $state, $ionicHistory, $ionicLoading ) {

 $scope.user = AuthService.user();
 $scope.form = {old_password: '', password : '', confirm_password :''}

 $scope.logout = function() {
      AuthService.logout();
    $state.go('login');
    $ionicHistory.clearHistory();
  }

 $scope.changePass = function() {
  $ionicLoading.show({
      template: 'Processing...',
    });
  UserService.changePass($scope.form).then(
    function(success) { $ionicLoading.hide();
      $scope.form = {old_password: '', password : '', confirm_password :''}
      $ionicPopup.alert({
           title: 'Success!',
           template: 'Password has been changed, please login with new password',
      });
      $scope.logout();
    }, function(error) { $ionicLoading.hide();
      $ionicPopup.alert({
                    title: 'Error!',
                    template: error.msg,
      });
    }
  );
 };

 $scope.$on('$ionicView.enter', function() {
   $scope.user = AuthService.user();
  $scope.form = {old_password: '', password : '', confirm_password :''}
 });
 
}]);