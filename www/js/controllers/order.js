app.controller('OrdersCtrl', ['$scope', 'OrderService', 'AuthService', '$ionicNavBarDelegate', '$ionicLoading', function($scope, OrderService, AuthService, $ionicNavBarDelegate, $ionicLoading) {

  $scope.user = AuthService.user();

  

  $scope.restriction = $scope.user.Restriction;



	$scope.orderService = OrderService;

  $scope.hasFiltered = false;

  $scope.listlength = 10;

  $scope.loadMore = function(){
    if (!$scope.orderService.orders){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }

    if ($scope.listlength < $scope.orderService.orders.length) {
      $scope.listlength+=5;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }


  $scope.addValueToSearch  = function() {
    var val =  document.querySelector('#orderSearch').value;
    $scope.search = val;
  }




  $scope.hasFocus = false;

   $scope.setFocus = function(){
    $scope.hasFocus = true;
    $ionicNavBarDelegate.title('');
    setTimeout(function(e) {
      document.querySelector('#orderSearch').focus();
    },100);
   

  };

  $scope.noFocus = function(){
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Order List');
    if(document.querySelector('#orderSearch').value.length > 0) {
      $scope.hasFiltered = true;
    };
    $scope.search = '';
  }

  $scope.$on('$ionicView.leave', function() {
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Order List');
    $scope.search = '';
  });

  $scope.$on('$ionicView.enter', function() {
    
    $ionicLoading.show({
      template: 'Loading...',
    });

    OrderService.updateFromServer().then(
      function() {
        $ionicLoading.hide();
      },
      function (error) {
        $ionicLoading.hide();
      }
    );
  });



}]);

app.controller('OrderDetailCtrl', ['$scope', 'OrderService', '$ionicNavBarDelegate', '$stateParams', function($scope, OrderService, $ionicNavBarDelegate, $stateParams) {

  $scope.orderService = OrderService;

  var orderIndex = $stateParams.order;

  $scope.order = OrderService.getSingle(orderIndex);


}]);