app.controller('CustomersCtrl', ['$scope', 'CustomerService', '$ionicNavBarDelegate', '$ionicLoading', function($scope, CustomerService, $ionicNavBarDelegate, $ionicLoading) {

	$scope.customerService = CustomerService;


  $scope.listlength = 10;

  $scope.loadMore = function(){
    if (!$scope.customerService.customers){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }

    if ($scope.listlength < $scope.customerService.customers.length) {
      $scope.listlength+=5;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }


  $scope.hasFiltered = false;

 


  $scope.addValueToSearch  = function() {
    var val =  document.querySelector('#customerSearch').value;
    $scope.search = val;
  }




  $scope.hasFocus = false;

   $scope.setFocus = function(){
    $scope.hasFocus = true;
    $ionicNavBarDelegate.title('');
    setTimeout(function(e) {
      document.querySelector('#customerSearch').focus();
    },100);
   

  };

  $scope.noFocus = function(){
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Customer List');
    if(document.querySelector('#customerSearch')) {
      if(document.querySelector('#customerSearch').value.length > 0) {
        $scope.hasFiltered = true;
      };
    }
    
    $scope.search = '';
  }

  $scope.$on('$ionicView.leave', function() {
    $scope.noFocus();
  });

  $scope.$on('$ionicView.enter', function() {
    $ionicLoading.show({
      template: 'Loading...'
    });
    $ionicNavBarDelegate.title('Customer List');
    CustomerService.updateFromServer().then(
      function() {
        $ionicLoading.hide();
      },
      function (error) {
         $ionicLoading.hide();
        
      }
    );
  });

}]);

app.controller('CustomerDetailCtrl', ['$scope', 'AuthService', 'CustomerService', '$stateParams', function($scope, AuthService, CustomerService, $stateParams) {
  $scope.customerIndex = $stateParams.customer;
  $scope.restriction = AuthService.user().Restriction;
  $scope.customer = CustomerService.getSingle($scope.customerIndex);
  $scope.customerService = CustomerService;
}]);

app.controller('customerOrdersCtrl', ['$scope', 'CustomerService', '$ionicNavBarDelegate', '$stateParams', '$ionicLoading', function($scope, CustomerService, $ionicNavBarDelegate, $stateParams, $ionicLoading) {

  $scope.customerService = CustomerService;

  $scope.customerIndex = $stateParams.customer;

  $scope.customer = CustomerService.getSingle($scope.customerIndex);

  $scope.customerOrders = CustomerService.customerOrders($scope.customer.CUSTOMER_NUMBER);


  $scope.hasFiltered = false;


  $scope.addValueToSearch  = function() {
    var val =  document.querySelector('#customerOrderSearch').value;
    $scope.search = val;
  }




  $scope.hasFocus = false;

   $scope.setFocus = function(){
    $scope.hasFocus = true;
    $ionicNavBarDelegate.title('');
    setTimeout(function(e) {
      document.querySelector('#customerOrderSearch').focus();
    },100);
   

  };

  $scope.noFocus = function(){
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Customer Detail');
    if(document.querySelector('#customerOrderSearch').value.length > 0) {
      $scope.hasFiltered = true;
    };
    $scope.search = '';
  }

  $scope.$on('$ionicView.leave', function() {
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Customer Detail');
    $scope.search = '';
  });

  $scope.$on('$ionicView.enter', function() {
    
    $ionicLoading.show({
      template: 'Loading...',
    });
    CustomerService.orderListFromServer($scope.customer.CUSTOMER_NUMBER).then(
      function(data) {
         $ionicLoading.hide();
         $scope.customerOrders = CustomerService.customerOrders($scope.customer.CUSTOMER_NUMBER);
      },
      function (error) {
        $ionicLoading.hide();
        
      }
    );
  });
  
}]);

app.controller('CustomerOrderDetailCtrl', ['$scope', 'CustomerService', '$ionicNavBarDelegate', '$stateParams', function($scope, CustomerService, $ionicNavBarDelegate, $stateParams) {

  $scope.customerService = CustomerService;

  var customerIndex = $stateParams.customer;

  var orderIndex = $stateParams.order;

  $scope.customer = CustomerService.getSingle(customerIndex);

  $scope.order = CustomerService.orderDetail($scope.customer.CUSTOMER_NUMBER, orderIndex);


}]);

app.controller('customerPaymentsCtrl', ['$scope', 'CustomerService', '$ionicNavBarDelegate', '$stateParams', '$ionicLoading', function($scope, CustomerService, $ionicNavBarDelegate, $stateParams, $ionicLoading) {

  $scope.customerService = CustomerService;

  $scope.customerIndex = $stateParams.customer;

  $scope.customer = CustomerService.getSingle($scope.customerIndex);
  

  $scope.customerPayments = CustomerService.customerPayments($scope.customer.CUSTOMER_NUMBER);


  $scope.hasFiltered = false;


  $scope.addValueToSearch  = function() {
    var val =  document.querySelector('#customerPaymentsSearch').value;
    $scope.search = val;
  }




  $scope.hasFocus = false;

   $scope.setFocus = function(){
    $scope.hasFocus = true;
    $ionicNavBarDelegate.title('');
    setTimeout(function(e) {
      document.querySelector('#customerPaymentsSearch').focus();
    },100);
   

  };

  $scope.noFocus = function(){
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Payments Detail');
    if(document.querySelector('#customerPaymentsSearch').value.length > 0) {
      $scope.hasFiltered = true;
    };
    $scope.search = '';
  }

  $scope.$on('$ionicView.leave', function() {
    $scope.hasFocus = false;
    $ionicNavBarDelegate.title('Payments Detail');
    $scope.search = '';
  });

  $scope.$on('$ionicView.enter', function() {
    
    $ionicLoading.show({
      template: 'Loading...',
    });
    CustomerService.paymentsFromServer($scope.customer.CUSTOMER_NUMBER).then(
      function(data) {
        $ionicLoading.hide();
        $scope.customerPayments = CustomerService.customerPayments($scope.customer.CUSTOMER_NUMBER);
      },
      function (error) {
        $ionicLoading.hide();
      }
    );
  });
  
}]);

app.controller('CustomerPaymentDetailCtrl', ['$scope', 'CustomerService', '$ionicNavBarDelegate', '$stateParams', function($scope, CustomerService, $ionicNavBarDelegate, $stateParams) {

  $scope.customerService = CustomerService;

  var customerIndex = $stateParams.customer;

  var paymentIndex = $stateParams.payment;

  $scope.customer = CustomerService.getSingle(customerIndex);

  $scope.payment = CustomerService.paymentDetail($scope.customer.CUSTOMER_NUMBER, paymentIndex);

}]);